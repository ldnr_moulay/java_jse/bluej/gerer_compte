
/**
 * Décrivez votre classe Compte_epargne ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Compte_epargne extends Banque
{
    private int taux_interet;
    
    
    public Compte_epargne(String epargne_adresse_banque, String epargne_nom_banque, String epargne_nom_client, String epargne_numero_compte, int epargne_solde_compte, int taux_interet){
        super(epargne_adresse_banque, epargne_nom_banque, epargne_nom_client, epargne_numero_compte, epargne_solde_compte);
        this.taux_interet = taux_interet;
    }
    
    public Compte_epargne(String epargne_adresse_banque, String epargne_nom_banque, String epargne_nom_client1, String epargne_nom_client2, String epargne_numero_compte, int epargne_solde_compte, int taux_interet){
        super(epargne_adresse_banque, epargne_nom_banque, epargne_nom_client1, epargne_nom_client2, epargne_numero_compte, epargne_solde_compte);
        this.taux_interet = taux_interet;
    }
    
    public int get_taux_interet(){
        return taux_interet;
    }
    
    public void isEpargne(){ 
        System.out.println("C'est un compte epargne");
    }
    

    public void afficher(String nom_client){
        super.afficher(nom_client);
        isEpargne();
        System.out.println("Taux d'interet : "+ " "+ taux_interet);
    }
}
