
/**
 * Décrivez votre classe Compte_cheque ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Compte_cheque extends Banque
{

    public Compte_cheque(String cheque_adresse_banque, String cheque_nom_banque, String cheque_nom_client, String cheque_numero_compte, int cheque_solde_compte){
        super(cheque_adresse_banque, cheque_nom_banque, cheque_nom_client, cheque_numero_compte, cheque_solde_compte);
    }
    
    public Compte_cheque(String cheque_adresse_banque, String cheque_nom_banque, String cheque_nom_client1, String cheque_nom_client2, String cheque_numero_compte, int cheque_solde_compte){
        super(cheque_adresse_banque, cheque_nom_banque, cheque_nom_client1, cheque_nom_client2, cheque_numero_compte, cheque_solde_compte);
    }
}
