
/**
 * Décrivez votre classe Banque ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Banque extends Client
{
    private String adresse_banque;
    private String nom_banque;
    private String numero_compte;
    private int solde_compte;
    
    public Banque(String adresse_banque, String nom_banque, String nom_client, String numero_compte, int solde_compte){
        super(nom_client);
        this.adresse_banque = adresse_banque;
        this.nom_banque = nom_banque;
        this.numero_compte = numero_compte;
        this.solde_compte = solde_compte;
        afficher(nom_client);
    }
    
    public Banque(String adresse_banque, String nom_banque, String nom_client1, String nom_client2, String numero_compte, int solde_compte){
        super(nom_client1, nom_client2);
        this.adresse_banque = adresse_banque;
        this.nom_banque = nom_banque;
        this.numero_compte = numero_compte;
        this.solde_compte = solde_compte;
        String nom_client= nom_client1 +" "+ "et"+" "+ nom_client2;
        afficher(nom_client);
    }
    
    public String get_nom_banque(){
        return nom_banque;
    }
    
    public String get_adresse_banque(){
        return adresse_banque;
    }
    
    public String get_numero_compte(){
        return numero_compte;
    }
    
    public int get_solde_compte(){
        return solde_compte;
    }
    
    public void afficher(String nom_client){
        System.out.println("Adresse banque : "+ adresse_banque+" "+"Nom banque : "+ nom_banque+" " +"Nom client : "+ nom_client+" "+"Numéro compte : "+ numero_compte+" " +"Solde compte : "+solde_compte);
    
    }
}
