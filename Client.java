
/**
 * Décrivez votre classe Client ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Client
{
    private String nom_client;
    
    public Client(String nom_client){
        this.nom_client = nom_client;
    }
    
    public Client(String nom_client1, String nom_client2){
        this.nom_client = nom_client1 + nom_client2;
    }
    
    public String get_nom_client(){
        return nom_client;
    }
}
